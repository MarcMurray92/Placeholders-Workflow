## Placehold.it URL Generator

![screenshot](https://gitlab.com/MarcMurray92/Placeholders-Workflow/raw/9d6f4705a632866fa715ff79048ea483751c166f/img.png "Just type _"phi 50x50"_ to generate, copy and paste a URL to a 50x50 placeholder image.")
